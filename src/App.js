
import './App.css';
import Navbar from './components/navbar/Navbar';
import { BrowserRouter,Routes,Route } from 'react-router-dom';
import ShopCategory from './pages/ShopCategory';
import Product from './pages/Product';
import Shop from './pages/Shop';
import Cart from './pages/Cart';
import LoginSignup from './pages/LoginSignup';
import Footer from './components/footer/Footer';


function App() {
  return (
    <div>
      
      <BrowserRouter>
      <Navbar/>
     
      <Routes>
        <Route path='/' element={<Shop/>}></Route>
        <Route path='/mens' element={<ShopCategory category="mens" />}></Route>
        <Route path='/womens' element={<ShopCategory category="womens"/>}></Route>
        <Route path='/kids' element={<ShopCategory category="kids"/>}></Route>
        <Route path="product" element={<Product/>}></Route>
         <Route path=':productId' element={<Product/>}></Route>
      
         <Route path='/cart' element={<Cart/>}></Route>
         <Route path='/login' element={<LoginSignup/>}></Route>
         
      </Routes>
      <Footer/>
      </BrowserRouter>
    </div>
  );
}

export default App;
